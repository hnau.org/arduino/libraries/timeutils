#include "TimeUtils.h"

String TimeUtils::millisecondsToString(timestamp milliseconds) {

    timestamp seconds = milliseconds / 1000;
    milliseconds -= seconds * 1000;
    timestamp minutes = seconds / 60;
    seconds -= minutes * 60;
    timestamp hours = minutes / 60;
    minutes -= hours * 60;
    timestamp days = hours / 24;
    hours -= days * 24;

    String result;

    if (days > 0) {
        result += days;
        result += "d ";
    }

    if (hours < 10) {
        result += "0";
    }
    result += hours;

    result += ":";

    if (minutes < 10) {
        result += "0";
    }
    result += minutes;

    result += ":";

    if (seconds < 10) {
        result += "0";
    }
    result += seconds;

    result += ".";

    if (milliseconds < 100) {
        result += "0";
    }
    if (milliseconds < 10) {
        result += "0";
    }
    result += milliseconds;

    return result;

}
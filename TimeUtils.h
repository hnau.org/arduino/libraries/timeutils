#ifndef TIME_UTILS_H
#define TIME_UTILS_H

#include <Arduino.h>

typedef uint32_t timestamp;


class TimeUtils {

public:
    static String millisecondsToString(timestamp milliseconds);


};


#endif //TIME_UTILS_H
